
static char *family_strings[] = {"Unknown", "legacy", "radeon",
				 "rv100", "rs100", "rv200", "rs200",
				 "r200", "rv250", "rv280", "rs300",
				 "r300", "r350",
				 "rv350", "rv380", "r420", "rv410", "rs400",
				 "rs480", "rv515", "r520", "rv530", "r580",
				 "rv560", "rv570", "rs600", "rs690", "rs740",
				 "r600", "r630", "rv610", "rv630", "rv670",
				 "rv620", "rv635", "rs780", "rs880",
				 "rv770", "rv730", "rv710", "rv740",
				 "cedar", "redwood", "juniper",
				 "cypress", "hemlock",
				 "palm",
				 "barts", "turks", "caicos" };

typedef enum {
    CHIP_FAMILY_UNKNOW,
    CHIP_FAMILY_LEGACY,
    CHIP_FAMILY_RADEON,
    CHIP_FAMILY_RV100,
    CHIP_FAMILY_RS100,    /* U1 (IGP320M) or A3 (IGP320)*/
    CHIP_FAMILY_RV200,
    CHIP_FAMILY_RS200,    /* U2 (IGP330M/340M/350M) or A4 (IGP330/340/345/350), RS250 (IGP 7000) */
    CHIP_FAMILY_R200,
    CHIP_FAMILY_RV250,
    CHIP_FAMILY_RS300,    /* RS300/RS350 */
    CHIP_FAMILY_RV280,
    CHIP_FAMILY_R300,
    CHIP_FAMILY_R350,
    CHIP_FAMILY_RV350,
    CHIP_FAMILY_RV380,    /* RV370/RV380/M22/M24 */
    CHIP_FAMILY_R420,     /* R420/R423/M18 */
    CHIP_FAMILY_RV410,    /* RV410, M26 */
    CHIP_FAMILY_RS400,    /* xpress 200, 200m (RS400) Intel */
    CHIP_FAMILY_RS480,    /* xpress 200, 200m (RS410/480/482/485) AMD */
    CHIP_FAMILY_RV515,    /* rv515 */
    CHIP_FAMILY_R520,    /* r520 */
    CHIP_FAMILY_RV530,    /* rv530 */
    CHIP_FAMILY_R580,    /* r580 */
    CHIP_FAMILY_RV560,   /* rv560 */
    CHIP_FAMILY_RV570,   /* rv570 */
    CHIP_FAMILY_RS600,
    CHIP_FAMILY_RS690,
    CHIP_FAMILY_RS740,
    CHIP_FAMILY_R600,    /* r600 */
    CHIP_FAMILY_R630,
    CHIP_FAMILY_RV610,
    CHIP_FAMILY_RV630,
    CHIP_FAMILY_RV670,
    CHIP_FAMILY_RV620,
    CHIP_FAMILY_RV635,
    CHIP_FAMILY_RS780,
    CHIP_FAMILY_RS880,
    CHIP_FAMILY_RV770,
    CHIP_FAMILY_RV730,
    CHIP_FAMILY_RV710,
    CHIP_FAMILY_RV740,
    CHIP_FAMILY_CEDAR,   /* evergreen */
    CHIP_FAMILY_REDWOOD,
    CHIP_FAMILY_JUNIPER,
    CHIP_FAMILY_CYPRESS,
    CHIP_FAMILY_HEMLOCK,
    CHIP_FAMILY_PALM,
    CHIP_FAMILY_BARTS,
    CHIP_FAMILY_TURKS,
    CHIP_FAMILY_CAICOS,
    CHIP_FAMILY_CAYMAN,
    CHIP_FAMILY_LAST
} RADEONChipFamily;

typedef struct {
    uint32_t pci_device_id;
    RADEONChipFamily chip_family;
    int mobility;
    int igp;
    int nocrtc2;
    int nointtvout;
    int singledac;
} RADEONCardInfo;

#include "radeon_chipinfo_gen.h"

#define IS_RV620(card_info) (card_info && card_info->chip_family >= CHIP_FAMILY_RV620)
#define IS_EG(card_info) (card_info && card_info->chip_family >= CHIP_FAMILY_CEDAR)

#define IS_DISPLAY_RADEON(card_info) (card_info && \
				      (card_info->chip_family >= CHIP_FAMILY_RADEON) && \
				      (card_info->chip_family <= CHIP_FAMILY_RS480))
#define IS_DISPLAY_AVIVO(card_info) (card_info && \
				     (card_info->chip_family >= CHIP_FAMILY_RV515) && \
				     (card_info->chip_family <= CHIP_FAMILY_RV670))
#define IS_DISPLAY_DCE3(card_info) (card_info && \
				    (card_info->chip_family >= CHIP_FAMILY_RV620) && \
				    (card_info->chip_family <= CHIP_FAMILY_RV740))
#define IS_DISPLAY_DCE4(card_info) (card_info && \
				    (card_info->chip_family >= CHIP_FAMILY_CEDAR) && \
				    (card_info->chip_family <= CHIP_FAMILY_PALM))
#define IS_DISPLAY_DCE5(card_info) (card_info && \
				    (card_info->chip_family >= CHIP_FAMILY_BARTS) && \
				    (card_info->chip_family <= CHIP_FAMILY_CAICOS))
