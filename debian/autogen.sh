#!/bin/sh
# Generate debian/changelog.upstream.
#
# Uses debian/changelog and the git revision log.
set -e

generated_changelog_needs_update() {
	test -e debian/changelog.upstream || return 0

	read line < debian/changelog.upstream || return 0
	ver=${line#Version }
	test "$ver" != "" || return 0

	read line < debian/changelog || return 0
	rhs=${line#*(}
	deb_ver=${rhs%)*}
	new_ver=${deb_ver%-*}

	test "$ver" != "$new_ver"
}

if ! generated_changelog_needs_update
then
	exit 0
fi
exec sh debian/changelog.upstream.sh
