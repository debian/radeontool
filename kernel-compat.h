
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>

#include <endian.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define ATOM_BIG_ENDIAN 0
#else
#define ATOM_BIG_ENDIAN 1
#endif

#define GFP_KERNEL 1

static inline void *kzalloc(size_t size, int flags) {
	void *pt;

	pt = calloc(1, size);
	return pt;
}

static inline void kfree(void *pt)
{
	free(pt);
}


#define printk printf

#define KERN_INFO ""
#define KERN_DEBUG ""

#define true 1
#define false 0

#define le16_to_cpu(x) (x)
#define le32_to_cpu(x) (x)
